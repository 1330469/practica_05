package com.cdm.upv.practica_05;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.content.Intent;

public class Main2Activity extends AppCompatActivity {

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int imagen = Integer.parseInt(message);

        image = (ImageView) findViewById(R.id.imageView1);

        switch (imagen){
            case 1:
                image.setImageResource(R.drawable.apple);
                break;
            case 2:
                image.setImageResource(R.drawable.grape);
                break;
            case 3:
                image.setImageResource(R.drawable.sandia);
                break;
            case 4:
                image.setImageResource(R.drawable.pineapple);
                break;
            default:
                image.setImageResource(R.drawable.apple);
                break;
        }

    }
}
